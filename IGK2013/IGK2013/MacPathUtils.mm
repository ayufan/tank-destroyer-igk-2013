//
//  MacPathUtils.mm\.h
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 07.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#import "PathUtils.h"
#import <Foundation/Foundation.h>
#import "includes.h"

std::string PathUtils::resourcePath(const std::string& fileName)
{
    NSBundle * bundle = [NSBundle mainBundle];
    
    NSString * resFile = [NSString stringWithUTF8String:fileName.c_str()];
    
    NSRange range = [resFile rangeOfString:@"."];

    NSString * base = [resFile substringWithRange:NSMakeRange(0, range.location)];
    NSString * ext = [resFile substringFromIndex:range.location + 1];
    
    NSString * path = [bundle pathForResource:base ofType:ext];
    
    if(path == nil)
        return fileName;
    
    std::string result([path UTF8String]);
    
    
    
    return result;
}

void PathUtils::init()
{
    NSBundle * mainBundle = [NSBundle mainBundle];
    

    CCFileUtils::sharedFileUtils()->addSearchPath([[mainBundle resourcePath] UTF8String]);
}
