#pragma once
#include "includes.h"

enum BlockType {
	BT_Wood = 0,
	BT_Brick = 1,
	BT_Stone = 2,
    BT_Plant = 3
};

class Block : public CCPhysicsSprite 
{
public:
	Block();
	~Block();
	static Block* create(BlockType type, float x, float y, int currentDestructionIndex = 0);
	void setupBlock(BlockType type, float x, float y, int currentDestructionIndex = 0);
	void setupGfx();
	void destroy();
	void spawnExplosion();
	void setupPhisics(b2World* world);
	void destroyPhysics();

public:
	BlockType blockType;
	float posX;
	float posY;
	int hp;
	int currentDestructionIndex;
	std::vector<Block*> childs;
	std::string fileName;
	b2World* physWorld;
	bool destroyed;
};
	
