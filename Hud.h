#pragma once
#include "includes.h"
#include "Player.h"

class Hud : public CCLayer
{
public:
	Hud();
	~Hud();
	static Hud *create(void);

	void createHudForPlayer(Player* player);
	void updateHud();

public:
	std::map<unsigned int, CCProgressTimer*> playersHp;
	std::map<unsigned int, CCProgressTimer*> playersMana;
	std::map<unsigned int, CCLabelTTF*> playersScore;
};