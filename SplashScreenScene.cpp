#include "StdAfx.h"
#include "SplashScreenScene.h"
#include "GameScene.h"
#include "MenuScene.h"


SplashScreenScene::SplashScreenScene(void)
{
}

SplashScreenScene::~SplashScreenScene(void)
{
}

CCScene* SplashScreenScene::scene()
{
	CCScene* scene = CCScene::create();
	SplashScreenScene* splashScreen = SplashScreenScene::create();
	scene->addChild(splashScreen);
	return scene;
}

bool SplashScreenScene::init()
{
	CCLayerColor::init();
	this->setColor(ccYELLOW);
	this->setTouchEnabled(true);

	CCSprite* pSprite = CCSprite::create("crysis.jpg");

	// position the sprite on the center of the screen
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	pSprite->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	pSprite->setScaleX(visibleSize.width / pSprite->getContentSize().width);
	pSprite->setScaleY(visibleSize.height / pSprite->getContentSize().height);
	
	// add the sprite as a child to this layer
	this->addChild(pSprite, 0);

	return true;
}

void SplashScreenScene::ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent )
{
	CCDirector::sharedDirector()->replaceScene(CCTransitionCrossFade::create(0.5f, MenuScene::scene()));
}