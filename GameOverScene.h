#pragma once
#include "cocos2d.h"

USING_NS_CC;

class GameOverScene : public CCLayer
{
public:
	GameOverScene(void);
	~GameOverScene(void);

	CREATE_FUNC(GameOverScene);
	static CCScene* scene();

	virtual bool init();
};

