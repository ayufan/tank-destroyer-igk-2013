#pragma once
#include "cocos2d.h"

USING_NS_CC;

class MenuScene : public CCLayer
{
public:
	MenuScene(void);
	~MenuScene(void);

	CREATE_FUNC(MenuScene);

	virtual bool init();

	static CCScene* scene();

	void startGame(CCObject* pSender);
	void about(CCObject* pSender);
};

