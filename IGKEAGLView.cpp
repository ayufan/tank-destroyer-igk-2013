#include "StdAfx.h"
#include "IGKEAGLView.h"
#include "GameScene.h"

LRESULT IGKEAGLView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	
	return CCEGLView::WindowProc(message, wParam, lParam);
}

IGKEAGLView* IGKEAGLView::sharedOpenGLView()
{
    static IGKEAGLView* s_pEglView = NULL;
    if (s_pEglView == NULL)
    {
        s_pEglView = new IGKEAGLView();
		if(!s_pEglView->Create())
		{
			delete s_pEglView;
			s_pEglView = NULL;
		}
    }

    return s_pEglView;
}