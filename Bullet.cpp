#include "StdAfx.h"
#include "Bullet.h"

Bullet::Bullet()
{
	destroyed = false;
	owner = NULL;
	motionStreak = NULL;
}

Bullet::~Bullet()
{
}

Bullet* Bullet::create(BulletType type)
{
	Bullet* pRet = new Bullet();
	pRet->bulletType = type;
	pRet->setupGfx();
	if (pRet && pRet->initWithFile(pRet->fileName.c_str()))
	{
		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}

void Bullet::setupGfx()
{
	switch(bulletType)
	{
	case BT_Rocket:
		fileName = "missle.png";
		break;
	case BT_Fire:
		fileName = "bullet.png";
		break;
  case BT_Bomb:
    fileName = "bomb.png";
    break;
	}
}

void Bullet::setupPhisics(b2World* world, float x, float y)
{
	physWorld = world;
	Box2DUtils::setupSprite(this, world, ccp(x, y));
	getB2Body()->SetBullet(true);
	getB2Body()->SetType(b2_dynamicBody);
	getB2Body()->SetUserData(this);

	b2MassData mass = {1000, b2Vec2(0,0), 1000.0f};
	getB2Body()->SetMassData(&mass);
}

void Bullet::destroyPhysics()
{
	Box2DUtils::destroySprite(this, physWorld);
}

void Bullet::destroyBullet()
{
	this->spawnExplosion();
	this->destroyPhysics();
	this->removeFromParent();
}

void Bullet::spawnExplosion(float scale)
{
	CCParticleSystem* fire = CCParticleSystemQuad::create("ExplodingRing1.plist");
	fire->setPosition(this->getPosition());
	fire->setAutoRemoveOnFinish(true);
	fire->setScale(scale);
	this->getParent()->addChild(fire, 1);
}

void Bullet::update(float dt)
{
	if(motionStreak) {
		motionStreak->setPosition( this->getPosition());
	}
}

void Bullet::startMotionStreak()
{
	motionStreak = CCMotionStreak::create(0.5f, 15, 10, ccWHITE, "smoke.png");
	this->getParent()->addChild(motionStreak, 0);
	this->scheduleUpdate();
}