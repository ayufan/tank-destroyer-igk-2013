#ifndef __IGK2013__PlayerManager__
#define __IGK2013__PlayerManager__

#include "Player.h"
#include <vector>
#include <map>

class PlayerManager
{
public:
    static PlayerManager& instance();

    typedef std::map<unsigned int, Player *> TPlayerMap;
    TPlayerMap m_players;

    Player * getPlayer(unsigned int playerId);
    void resetPlayers();
    void addPlayer(Player * p);
};

#endif /* defined(__IGK2013__PlayerManager__) */
