#include "StdAfx.h"
#include "PlayerManager.h"
#include <memory>

PlayerManager& PlayerManager::instance() {
    static PlayerManager manager;
    return manager;
}

Player * PlayerManager::getPlayer(unsigned int playerId)
{
    return m_players[playerId];
}

void PlayerManager::resetPlayers()
{
  /*
    for(TPlayerMap::iterator it = m_players.begin() ; it != m_players.end() ; ++it) {
        delete it->second;
    }
    */
    m_players.clear();
}

void PlayerManager::addPlayer(Player * p)
{
    m_players[p->m_playerId] = p;
}

