#include "StdAfx.h"
#include "AboutScene.h"


AboutScene::AboutScene(void)
{
}


AboutScene::~AboutScene(void)
{
}

CCScene* AboutScene::scene()
{
	CCScene* scene = CCScene::create();
	scene->addChild(AboutScene::create());
	return scene;
}

bool AboutScene::init()
{
	CCLayer::init();
	this->setTouchEnabled(true);

    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    CCSprite* background = CCSprite::create("about.jpg");
//    background->setAnchorPoint(CCPointZero);
//    background->setPosition(CCPointZero);
//    
    
    background->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	background->setScaleX(visibleSize.width / background->getContentSize().width);
	background->setScaleY(visibleSize.height / background->getContentSize().height);
    
    
    addChild(background);

	return true;
}

void AboutScene::ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent )
{
	CCDirector::sharedDirector()->popScene();
}



