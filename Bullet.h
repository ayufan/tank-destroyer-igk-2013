#pragma once
#include "includes.h"

enum BulletType
{
	BT_Rocket = 0,
	BT_Fire = 1,
  BT_Bomb = 2
};

class Bullet : public CCPhysicsSprite
{
public:
	Bullet();
	~Bullet();

	static Bullet* create(BulletType type);
	void setupGfx();

	void update(float dt);

	void setupPhisics(b2World* world, float x, float y);
	void destroyPhysics();

	void destroyBullet();
	void spawnExplosion(float scale = 0.2f);

	void startMotionStreak();
    
    float getDamage() const {
        if(bulletType == BT_Rocket)
            return 30;
        if(bulletType == BT_Fire)
            return 10;
        return 5;
    }

public:
	BulletType bulletType;
	std::string fileName;
	b2World* physWorld;
	bool destroyed;
	class Player* owner;
	CCMotionStreak* motionStreak;
};