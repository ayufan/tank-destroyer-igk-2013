#include "StdAfx.h"
#include "PhysicsUtils.h"

void Box2DUtils::setupSprite( CCPhysicsSprite* sprite, b2World* world, CCPoint p)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
    bodyDef.userData = (void *)sprite;
    
	b2Body *body = world->CreateBody(&bodyDef);


	// Define another box shape for our dynamic body.
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox((sprite->getContentSize().width * sprite->getScaleX()) / 2 / PTM_RATIO, 
		(sprite->getContentSize().height * sprite->getScaleY()) / 2 / PTM_RATIO);

	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;    
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	body->CreateFixture(&fixtureDef);    

	sprite->setB2Body(body);
	sprite->setPTMRatio(PTM_RATIO);
	sprite->setPosition( ccp( p.x, p.y) );
}

void Box2DUtils::destroySprite(CCPhysicsSprite* sprite, b2World* world)
{
	world->DestroyBody(sprite->getB2Body());
}



