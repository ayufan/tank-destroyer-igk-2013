#include "StdAfx.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "AboutScene.h"


MenuScene::MenuScene(void)
{
}


MenuScene::~MenuScene(void)
{
}

bool MenuScene::init()
{
	CCLayer::init();
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    CCSprite* background = CCSprite::create("menu.png");
//    background->setAnchorPoint(CCPointZero);
//    background->setPosition(CCPointZero);
    
    background->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	background->setScaleX(visibleSize.width / background->getContentSize().width);
	background->setScaleY(visibleSize.height / background->getContentSize().height);
    
    
    addChild(background);
    


    

	CCMenuItemLabel* start = CCMenuItemLabel::create(
		CCLabelTTF::create("start", "Stencil Std", 100), 
		this, menu_selector(MenuScene::startGame));
	
	CCMenuItemLabel* about = CCMenuItemLabel::create(
		CCLabelTTF::create("about", "Stencil Std", 50), 
		this, menu_selector(MenuScene::about));

	CCMenu* menu = CCMenu::create(start, about, NULL);
	menu->alignItemsVertically();
	addChild(menu);

	return true;
}

CCScene* MenuScene::scene()
{
	CCScene* scene = CCScene::create();
	scene->addChild(MenuScene::create());
	return scene;
}

void MenuScene::startGame(CCObject* pSender)
{
	CCDirector::sharedDirector()->pushScene(CCTransitionCrossFade::create(0.5f, GameScene::scene(1)));
}

void MenuScene::about( CCObject* pSender )
{
	CCDirector::sharedDirector()->pushScene(CCTransitionCrossFade::create(0.5f, AboutScene::scene()));
}
