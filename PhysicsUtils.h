#pragma once
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "VisibleRect.h"
#include "cocos-ext.h"
#include "constants.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Box2DUtils
{
public:
	static void setupSprite( CCPhysicsSprite* sprite, b2World* world, CCPoint p);
	static void destroySprite(CCPhysicsSprite* sprite, b2World* world);
};
