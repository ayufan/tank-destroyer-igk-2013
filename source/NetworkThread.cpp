//
//  NetworkThread.cpp
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 06.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#include "StdAfx.h"
#ifdef _WIN32
#include <WinSock.h>
#endif
#include "NetworkThread.h"
#include <cocos2d.h>
#include <assert.h>

NetworkThread& NetworkThread::instance()
{
    static NetworkThread s_thread;
    return s_thread;
}

NetworkThread::NetworkThread()
{
    pthread_mutex_init(&queue_lock, NULL);
    pthread_create(&thread_ref, NULL, &NetworkThread::thread_entry, this);
}

NetworkThread::~NetworkThread()
{
    pthread_kill(thread_ref, 0);
}

void NetworkThread::start()
{
    CCLOG("Network thread started");

#ifdef _WIN32
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        printf("WSAStartup failed with error: %d\n", err);
        return;
    }
#endif
    
    int sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    assert(sock_fd > 0);
    
    sockaddr_in sock_addr;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_addr.s_addr = INADDR_ANY;
    sock_addr.sin_port = htons(12900);
    
    int result = ::bind(sock_fd, (sockaddr *)&sock_addr, sizeof(sock_addr));
    
    assert(result == 0);
    
    int yes = 1;
    result = setsockopt(sock_fd, SOL_SOCKET, SO_BROADCAST, (char *)&yes, sizeof(yes));
    assert(result == 0);
    
    CCLOG("fd=%d", sock_fd);
    
    for(;;) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(sock_fd, &fds);
        
        int rv = select(sock_fd +1, &fds, NULL, NULL, NULL);
        //CCLOG("rv=%d", rv);
        if(rv == EINTR)
            continue;
        
        if(FD_ISSET(sock_fd, &fds)) {
            char buffer[4096];
            sockaddr_in remote_sa;
            socklen_t slen = sizeof(remote_sa);
            int recv_size = recvfrom(sock_fd, buffer, sizeof(buffer), 0, (sockaddr *)&remote_sa, &slen);
            //CCLOG("Packet recv = %d", recv_size);
            if(recv_size == sizeof(NetworkPacket)) {
                NetworkPacket packet;
                memcpy(&packet, buffer, sizeof(packet));
                
                //CCLOG("P:%d X:%f Y:%f AX:%f AY:%f AZ:%f", packet.player, packet.x, packet.y, packet.ax, packet.ay, packet.az);
                
                pthread_mutex_lock(&queue_lock);
                if(packetQueue.size() > 16) {
                    packetQueue.erase(packetQueue.begin(), packetQueue.begin() + packetQueue.size() - 16);
                }
                packetQueue.push_back(packet);
                pthread_mutex_unlock(&queue_lock);
            }
        }
    }
}

void * NetworkThread::thread_entry(void * ptr)
{
    reinterpret_cast<NetworkThread *>(ptr)->start();
    return 0;
}

std::vector<NetworkPacket> NetworkThread::getPackets()
{
    pthread_mutex_lock(&queue_lock);
    std::vector<NetworkPacket> packets;
    packets.reserve(packetQueue.size());
    while(!packetQueue.empty()) {
        packets.push_back(packetQueue.front());
        packetQueue.erase(packetQueue.begin());
    }
    pthread_mutex_unlock(&queue_lock);
    return packets;
}

void NetworkThread::pushPacket( NetworkPacket p )
{
    pthread_mutex_lock(&queue_lock);
    packetQueue.push_back(p);
    pthread_mutex_unlock(&queue_lock);
}
