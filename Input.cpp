
#include "StdAfx.h"
//#include "Common.h"
#include "Input.h"
#include <string.h>

#define check(a) a

Input* Input::instance() {
	static Input* sharedInstance;
	if(!sharedInstance) {
		sharedInstance = new Input();
#ifdef __APPLE__
        memset(sharedInstance->tempKeys, 0, sizeof(sharedInstance->tempKeys));
        memset(sharedInstance->keys, 0, sizeof(sharedInstance->keys));
#endif
	}
	return sharedInstance;
}

#ifdef _WIN32
void Input::init(HINSTANCE appInstance, bool isExclusive)
{
	// wyczyszczenie stanu klawiszy
	for(int i = 0; i < 256; i++) {
		keys[i] = tempKeys[i] = false;
	}
}
#endif

void Input::update()
{
#ifdef _WIN32
	// update klawiszy
	for (int i = 0; i < 256; i++) 
	{
		bool state  = static_cast<bool>(GetAsyncKeyState(i));
		tempKeys[i] = (state != keys[i]);
		keys[i]     = state;
	}
#endif
}

#ifdef __APPLE__
void Input::setKey(int key, bool down)
{
    tempKeys[key] = (down != keys[key]);
    keys[key] = down;
}
#endif
