//
//  PowerUp.h
//  IGK2013
//
//  Created by Jaroslaw Pelczar on 07.04.2013.
//  Copyright (c) 2013 Kryzys. All rights reserved.
//

#ifndef __IGK2013__PowerUp__
#define __IGK2013__PowerUp__

#include "includes.h"

class Player;

class PowerUp : public CCPhysicsSprite
{
public:
    PowerUp();
    ~PowerUp();
    
    static PowerUp * create(const std::string& imageName, void (* action)(Player *));

    void onHide(CCNode * node);

    void collidedWithPlayer(Player * p);
    
    bool aready_used;
    
    void setupPhisics(b2World* world, float xPos, float yPos);
    void destroyPhysics();
    
private:
    void (* mAction)(Player *);
    b2World * physWorld;
};

#endif /* defined(__IGK2013__PowerUp__) */
